-printmapping out.map

-optimizationpasses 5
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontpreverify
-verbose
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*,!code/allocation/variable
-ignorewarnings
-keepparameternames

-keepattributes Signature, Exceptions, *Annotation*, JavascriptInterface

-keep public class ir.adad.fullscreen.AdadFullscreenBannerAd {
    public static void prepare(android.content.Context, java.lang.String);
    public static void prepare(android.content.Context, java.lang.String, ir.adad.ad.AdadAdListener);
    public static boolean isReady();
    public static boolean isVisible();
    public static void show(android.app.Activity);

    public static void destroy();

}

# Android suff
-dontnote **apache.http.**
-dontwarn android.support.v4.**, androidx.**
-keepclassmembers class * implements android.os.Parcelable {
      public static final android.os.Parcelable$Creator *;
}

-dontnote android.net.http.*
-dontnote org.apache.commons.codec.**
-dontnote org.apache.http.**
-keep public class android.webkit.JavascriptInterface {}

# Support for Android Advertiser ID.
-keep class com.google.android.gms.common.GooglePlayServicesUtil {*;}
-keep class com.google.android.gms.ads.identifier.AdvertisingIdClient {*;}
-keep class com.google.android.gms.ads.identifier.AdvertisingIdClient$Info {*;}


# Support for Google Play Services
# http://developer.android.com/google/play-services/setup.html
-keep class * extends java.util.ListResourceBundle {
    protected Object[][] getContents();
}

-keep public class com.google.android.gms.common.internal.safeparcel.SafeParcelable {
    public static final *** NULL;
}

-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
    @com.google.android.gms.common.annotation.KeepName *;
}
