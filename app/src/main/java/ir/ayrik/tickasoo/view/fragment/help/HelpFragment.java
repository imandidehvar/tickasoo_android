package ir.ayrik.tickasoo.view.fragment.help;

import androidx.fragment.app.Fragment;

public class HelpFragment extends Fragment {
    private static HelpFragment instance;

    public static HelpFragment getInstance() {
        if(instance == null)
            instance = new HelpFragment();

        return instance;
    }
}
