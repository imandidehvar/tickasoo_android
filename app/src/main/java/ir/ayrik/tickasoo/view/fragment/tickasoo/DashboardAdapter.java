package ir.ayrik.tickasoo.view.fragment.tickasoo;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import ir.ayrik.tickasoo.DataHolder;
import ir.ayrik.tickasoo.databinding.GridviewItemIconBinding;
import ir.ayrik.tickasoo.model.Menu;

import static ir.ayrik.tickasoo.databinding.GridviewItemIconBinding.inflate;

public class DashboardAdapter extends BaseAdapter {
    private List<Menu> menus;
    private Context context;

    public DashboardAdapter(Context context, List<Menu> menus) {
        this.context = context;
        this.menus = menus;
    }

    @Override
    public int getCount() {
        return menus.size();
    }

    @Override
    public Menu getItem(int position) {
        return menus.get(position);
    }

    @Override
    public long getItemId(int position) {
        return menus.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        GridviewItemIconBinding binding = inflate(LayoutInflater.from(context));
        final Menu item = getItem(position);

        if(!item.getBackground().isEmpty() && item.getBackground().startsWith("http")){
            Picasso.get().load((getItem(position).getBackground())).into(binding.imgPicture);
        } else if (item.getBackground().startsWith("#")) {
            binding.imgPicture.setBackgroundColor(Color.parseColor(getItem(position).getBackground()));
            binding.lblText.setText(getItem(position).getName());
        }

        binding.viewItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (!item.getSource().isEmpty() && item.getSource().startsWith("http")) {

                    } else {
                        Class ref = Class.forName(item.getSource());
                        DataHolder.getCurrent().menu = item;
                    }
                } catch (ClassNotFoundException ex) {
                    ex.printStackTrace();
                    Toast.makeText(context,"این قابلیت در نسخه شما پشتیانی نمیشود. لطفا نسخه خود را ارتقا دهید.", Toast.LENGTH_LONG).show();
                }
            }
        });

        return binding.getRoot();
    }
}
