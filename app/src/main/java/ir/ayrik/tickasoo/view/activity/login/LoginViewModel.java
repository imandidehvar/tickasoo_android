package ir.ayrik.tickasoo.view.activity.login;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.Map;

import ir.ayrik.tickasoo.data.remote.APIProvider;
import ir.ayrik.tickasoo.model.Account;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginViewModel extends ViewModel {
    static final int REQUEST_CODE = 1;
    static final int LOGIN = 2;

    private MutableLiveData<Integer> viewType = new MutableLiveData<>();
    private MutableLiveData<Account> profile = new MutableLiveData<>();
    private MutableLiveData<String> token = new MutableLiveData<>();

    public MutableLiveData<Account> loginUser(Map map) {
        APIProvider.getInstance()
                .getService()
                .loginUser(map)
                .enqueue(new Callback<Account>() {
                    @Override
                    public void onResponse(Call<Account> call, Response<Account> response) {
                        if (response.isSuccessful()) {
                            profile.postValue(response.body());
                        }
                    }
                    @Override
                    public void onFailure(Call<Account> call, Throwable t) { }
                });
        return profile;
    }

    public MutableLiveData<String> verifyUser(Map map) {
        APIProvider.getInstance()
                .getService()
                .verifyUser(map)
                .enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if (response.isSuccessful()) {
                            token.postValue(response.body());
                        }
                    }
                    @Override
                    public void onFailure(Call<String> call, Throwable t) { }
                });
        return token;
    }

    public MutableLiveData<Integer> getState() {
        return viewType;
    }

    public void setState(int state) {
        viewType.postValue(state);
    }
}
