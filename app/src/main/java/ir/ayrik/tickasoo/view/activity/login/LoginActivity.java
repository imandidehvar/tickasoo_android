package ir.ayrik.tickasoo.view.activity.login;

import android.os.CountDownTimer;
import android.view.View;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.pixplicity.easyprefs.library.Prefs;

import java.util.HashMap;
import java.util.Map;

import ir.ayrik.tickasoo.R;
import ir.ayrik.tickasoo.component.AyrikActivity.AyrikActivity;
import ir.ayrik.tickasoo.databinding.ActivityLoginBinding;
import ir.ayrik.tickasoo.model.Account;
import ir.ayrik.tickasoo.util.Constraints;
import ir.ayrik.tickasoo.view.activity.dashboard.DashboardActivity;

public class LoginActivity extends AyrikActivity<LoginViewModel, ActivityLoginBinding> {
    CountDownTimer resendTimer = new CountDownTimer(90000, 1000) {
        @Override
        public void onTick(long l) {
            try {
                String minute = String.valueOf((l / 1000) / 60).length() == 1 ? "0" + (l / 1000) / 60 : String.valueOf((l / 1000) / 60);
                String second = String.valueOf((l / 1000) % 60).length() == 1 ? "0" + (l / 1000) % 60 : String.valueOf((l / 1000) % 60);
                String time = minute + ":" + second;
                binding.lblResend.setText(time);
                binding.lblResend.setEnabled(false);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        @Override
        public void onFinish() {
            binding.lblResend.setText("ارسال مجدد");
            binding.lblResend.setEnabled(true);
        }
    };

    @Override
    public int layout() {
        return R.layout.activity_login;
    }

    @Override
    protected void onReady() {
        model = ViewModelProviders.of(this).get(LoginViewModel.class);
        model.setState(LoginViewModel.REQUEST_CODE);

        binding.setEvents(new Events());

        model.getState().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                changeView();
            }
        });
    }

    private void changeView() {
        if (model.getState().getValue() == LoginViewModel.REQUEST_CODE) {
            binding.viewPassword.setVisibility(View.GONE);
            binding.txtPhone.setEnabled(true);
            binding.btnLogin.setText("درخواست کد تایید هویت");
        } else if (model.getState().getValue() == LoginViewModel.LOGIN) {
            binding.txtPhone.setEnabled(false);
            binding.viewPassword.setVisibility(View.VISIBLE);
            binding.btnLogin.setText("ورود به تیکاسو");
            resendTimer.start();
            showToast("جهت تغییر شماره دکمه بازگشت را فشار دهید.");
        }
    }

    private void login(String phone) {
        Map<String, String> request = new HashMap<>();
        request.put("phone", phone);
        model.loginUser(request).observe(this, new Observer<Account>() {
            @Override
            public void onChanged(Account account) {
                if (account != null && account.getId() != 0) {
                    Prefs.putInt(Constraints.STUDENT_ID, account.getId());
                    Prefs.putString(Constraints.STUDENT_PHONE, account.getPhone());
                    Prefs.putString(Constraints.STUDENT_NAME, account.getName());

                    if (account.getRole() != null) {
                        Prefs.putInt(Constraints.ROLE_ID, account.getRole().getId());
                        Prefs.putString(Constraints.ROLE_NAME, account.getRole().getName());
                    }

                    if (account.getMajor() != null) {
                        Prefs.putInt(Constraints.MAJOR_ID, account.getMajor().getId());
                        Prefs.putString(Constraints.MAJOR_NAME, account.getMajor().getName());
                    }

                    if (account.getSchool() != null) {
                        Prefs.putInt(Constraints.SCHOOL_ID, account.getSchool().getId());
                        Prefs.putString(Constraints.SCHOOL_NAME, account.getSchool().getName());
                    }

                    if (account.getCity() != null) {
                        Prefs.putInt(Constraints.CITY_ID, account.getCity().getId());
                        Prefs.putString(Constraints.CITY_NAME, account.getCity().getName());
                    }

                    model.setState(LoginViewModel.LOGIN);
                }
            }
        });
    }

    private void verify(String phone, String code) {
        Map<String, String> request = new HashMap<>();
        request.put("phone", phone);
        request.put("code", code);
        model.verifyUser(request).observe(this, new Observer<String>() {
            @Override
            public void onChanged(String token) {
                if (!token.isEmpty()) {
                    Prefs.putString("USER_TOKEN", token);
                    Prefs.putBoolean("USER_AUTHENTICATED", true);
                    startActivityWithFinish(DashboardActivity.class);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (model.getState().getValue() == LoginViewModel.REQUEST_CODE) {
            setOnBackPressActions(true, true);
            super.onBackPressed();
        } else {
            model.setState(LoginViewModel.REQUEST_CODE);
            resendTimer.cancel();
            binding.txtPassword.setText("");
        }
    }

    public class Events {
        public void login_click() {
            if (model.getState().getValue() == LoginViewModel.REQUEST_CODE) {
                String phone = (!binding.txtPhone.getText().toString().isEmpty()
                        && binding.txtPhone.getText().length() == 11
                        && binding.txtPhone.getText().toString().startsWith("09")) ? binding.txtPhone.getText().toString() : null;
                if (phone != null)
                    login(phone);
                else
                    showToast("لطفا شماره تلفن خود را صحیح وارد کنید");
            } else if (model.getState().getValue() == LoginViewModel.LOGIN) {
                String phone = (!binding.txtPhone.getText().toString().isEmpty()
                        && binding.txtPhone.getText().length() == 11
                        && binding.txtPhone.getText().toString().startsWith("09")) ? binding.txtPhone.getText().toString() : null;
                String code = (!binding.txtPassword.getText().toString().isEmpty()) ? binding.txtPassword.getText().toString() : null;
                if (binding.txtPhone.getText() != null && binding.txtPassword.getText() != null)
                    verify(phone, code);
                else
                    showToast("لطفا کد تایید را وارد فرمایید.");

            }
        }
    }
}


