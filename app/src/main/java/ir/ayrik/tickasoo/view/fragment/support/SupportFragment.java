package ir.ayrik.tickasoo.view.fragment.support;

import androidx.fragment.app.Fragment;

public class SupportFragment extends Fragment {
    private static SupportFragment instance;

    public static SupportFragment getInstance() {
        if(instance == null)
            instance = new SupportFragment();

        return instance;
    }
}
