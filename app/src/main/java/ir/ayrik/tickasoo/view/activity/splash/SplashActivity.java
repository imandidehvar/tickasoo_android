package ir.ayrik.tickasoo.view.activity.splash;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.pixplicity.easyprefs.library.Prefs;

import ir.ayrik.tickasoo.R;
import ir.ayrik.tickasoo.component.AyrikActivity.AyrikActivity;
import ir.ayrik.tickasoo.databinding.ActivitySplashBinding;
import ir.ayrik.tickasoo.model.Setting;
import ir.ayrik.tickasoo.util.Constraints;
import ir.ayrik.tickasoo.view.activity.dashboard.DashboardActivity;
import ir.ayrik.tickasoo.view.activity.login.LoginActivity;

public class SplashActivity extends AyrikActivity<SplashViewModel, ActivitySplashBinding>
{
    @Override
    public int layout() {
        return R.layout.activity_splash;
    }

    @Override
    protected void onReady() {
        model = ViewModelProviders.of(this).get(SplashViewModel.class);

        model.getSetting("1.0").observe(this, new Observer<Setting>() {
            @Override
            public void onChanged(Setting setting) {
                if(setting.isAvailable() && Prefs.getBoolean(Constraints.AUTHENTICATED, false))
                    startActivity(DashboardActivity.class);
                else
                    startActivity(LoginActivity.class);
            }
        });
    }
}
