package ir.ayrik.tickasoo.view.activity.dashboard;

import android.annotation.SuppressLint;
import android.view.MenuItem;
import android.view.View;

import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.pixplicity.easyprefs.library.Prefs;

import java.lang.reflect.Field;

import ir.ayrik.tickasoo.R;
import ir.ayrik.tickasoo.component.AyrikActivity.AyrikActivity;
import ir.ayrik.tickasoo.databinding.ActivityDashboardBinding;
import ir.ayrik.tickasoo.util.Constraints;
import ir.ayrik.tickasoo.view.fragment.account.AccountFragment;
import ir.ayrik.tickasoo.view.fragment.help.HelpFragment;
import ir.ayrik.tickasoo.view.fragment.setting.SettingFragment;
import ir.ayrik.tickasoo.view.fragment.support.SupportFragment;
import ir.ayrik.tickasoo.view.fragment.tickasoo.TickasooFragment;

public class DashboardActivity extends AyrikActivity<DashboardViewModel, ActivityDashboardBinding> {
    private BottomNavigationView navigation;
    private Fragment lastTab, activeTab;
    private boolean helpAded = false;
    private boolean settingAded = false;
    private boolean tickasooAded = false;
    private boolean accountAded = false;
    private boolean supportAded = false;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.tickasoo_main:
                    LoadFragment(ApplicationFragments.TickasooFragment);
                    return true;
                case R.id.tickasoo_help:
                    LoadFragment(ApplicationFragments.HelpFragment);
                    return true;
                case R.id.tickasoo_setting:
                    LoadFragment(ApplicationFragments.SettingFragment);
                    return true;
                case R.id.tickasoo_support:
                    LoadFragment(ApplicationFragments.SupportFragment);
                    return true;
                case R.id.tickasoo_account:
                    LoadFragment(ApplicationFragments.AccountFragment);
                    return true;
            }
            return false;
        }
    };
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener_JustAccount = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(MenuItem item) {
            if (item.getItemId() == R.id.tickasoo_account) {
                LoadFragment(ApplicationFragments.AccountFragment);
                return true;
            }
            showToast("برای استفاده از تیکاسو باید ابتدا حساب کاربری خود را تکمیل کنید.");
            return false;
        }
    };

    @Override
    public int layout() {
        return R.layout.activity_dashboard;
    }

    @Override
    protected void onReady() {
        navigation = binding.navigationView;

        if (Prefs.getInt(Constraints.ROLE_ID, 0) != 0) {
            navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener_JustAccount);
            navigation.setSelectedItemId(R.id.tickasoo_account);
            LoadFragment(ApplicationFragments.AccountFragment);
        } else {
            navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
            navigation.setSelectedItemId(R.id.tickasoo_main);
            LoadFragment(ApplicationFragments.TickasooFragment);
        }

        disableShiftMode(navigation);
    }

    private void LoadFragment(ApplicationFragments fragment) {
        Fragment loadFragment = null;
        switch (fragment) {
            case HelpFragment:
                loadFragment = HelpFragment.getInstance();
                if (!helpAded)
                    getSupportFragmentManager().beginTransaction().add(R.id.container, loadFragment).hide(loadFragment).commit();
                helpAded = true;
                break;
            case SettingFragment:
                loadFragment = SettingFragment.getInstance();
                if (!settingAded)
                    getSupportFragmentManager().beginTransaction().add(R.id.container, loadFragment).hide(loadFragment).commit();
                settingAded = true;
                break;
            case TickasooFragment:
                loadFragment = TickasooFragment.getInstance();
                if (!tickasooAded)
                    getSupportFragmentManager().beginTransaction().add(R.id.container, loadFragment).hide(loadFragment).commit();
                tickasooAded = true;
                break;
            case AccountFragment:
                loadFragment = AccountFragment.getInstance();
                if (!accountAded)
                    getSupportFragmentManager().beginTransaction().add(R.id.container, loadFragment).hide(loadFragment).commit();
                accountAded = true;
                break;
            case SupportFragment:
                loadFragment = SupportFragment.getInstance();
                if (!supportAded)
                    getSupportFragmentManager().beginTransaction().add(R.id.container, loadFragment).hide(loadFragment).commit();
                supportAded = true;
                break;
        }
        lastTab = activeTab;
        activeTab = loadFragment;

        if (lastTab != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .hide(lastTab)
                    .show(loadFragment)
                    .commit();
        } else {
            getSupportFragmentManager()
                    .beginTransaction()
                    .show(loadFragment)
                    .commit();
        }

    }

    @SuppressLint("RestrictedApi")
    private void disableShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                item.setShifting(false);
                item.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                // set once again checked value, so view will be updated
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException ignored) {
        } catch (IllegalAccessException ignored) {
        }
    }

    private enum ApplicationFragments {
        HelpFragment,
        SettingFragment,
        TickasooFragment,
        SupportFragment,
        AccountFragment
    }
}
