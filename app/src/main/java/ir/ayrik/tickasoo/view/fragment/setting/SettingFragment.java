package ir.ayrik.tickasoo.view.fragment.setting;

import androidx.fragment.app.Fragment;

public class SettingFragment extends Fragment {
    private static SettingFragment instance;

    public static SettingFragment getInstance() {
        if(instance == null)
            instance = new SettingFragment();

        return instance;
    }
}
