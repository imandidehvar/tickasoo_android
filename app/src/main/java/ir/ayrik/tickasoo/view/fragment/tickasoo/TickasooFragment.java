package ir.ayrik.tickasoo.view.fragment.tickasoo;

import android.view.View;

import com.pixplicity.easyprefs.library.Prefs;

import ir.ayrik.tickasoo.R;
import ir.ayrik.tickasoo.component.AyrikFragment.AyrikFragment;
import ir.ayrik.tickasoo.databinding.FragmentTickasooBinding;
import ir.ayrik.tickasoo.util.Constraints;

public class TickasooFragment extends AyrikFragment<TickasooViewModel, FragmentTickasooBinding> {
    private static TickasooFragment instance;

    public static TickasooFragment getInstance() {
        if (instance == null)
            instance = new TickasooFragment();


        return instance;
    }

    @Override
    protected int layout() {
        return R.layout.fragment_tickasoo;
    }

    @Override
    protected View onReady(View view) {
        int roleId = Prefs.getInt(Constraints.ROLE_ID, 0);
        return view;
    }
}