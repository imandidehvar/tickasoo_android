package ir.ayrik.tickasoo.view.fragment.plugin.main;

import android.view.View;

import ir.ayrik.tickasoo.component.AyrikFragment.AyrikFragment;
import ir.ayrik.tickasoo.databinding.FragmentMainBinding;

public class MainFragment extends AyrikFragment<MainViewModel, FragmentMainBinding> {
    @Override
    protected int layout() {
        return 0;
    }

    @Override
    protected View onReady(View view) {
        return null;
    }
}
