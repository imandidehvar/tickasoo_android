package ir.ayrik.tickasoo.view.fragment.account;

import android.view.View;

import ir.ayrik.tickasoo.R;
import ir.ayrik.tickasoo.component.AyrikFragment.AyrikFragment;
import ir.ayrik.tickasoo.databinding.FragmentAccountBinding;


public class AccountFragment extends AyrikFragment<AccountViewModel, FragmentAccountBinding>  {
    private static AccountFragment instance;

    public static AccountFragment getInstance() {
        if(instance == null)
            instance = new AccountFragment();

        return instance;
    }

    @Override
    protected int layout() {
        return R.layout.fragment_account;
    }

    @Override
    protected View onReady(View view) {
        return null;
    }
}
