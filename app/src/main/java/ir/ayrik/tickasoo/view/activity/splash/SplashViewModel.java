package ir.ayrik.tickasoo.view.activity.splash;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import ir.ayrik.tickasoo.data.remote.APIProvider;
import ir.ayrik.tickasoo.model.Account;
import ir.ayrik.tickasoo.model.Setting;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashViewModel extends ViewModel {
    private MutableLiveData<Setting> setting = new MutableLiveData<>();

    public MutableLiveData<Setting> getSetting(String version) {
        APIProvider.getInstance()
                .getService()
                .getSetting()
                .enqueue(new Callback<Setting>() {
                    @Override
                    public void onResponse(@NonNull Call<Setting> call, @NonNull Response<Setting> response) {
                        if(response.isSuccessful())
                            setting.postValue(response.body());
                    }
                    @Override
                    public void onFailure(@NonNull Call<Setting> call, @NonNull Throwable t) { }
                });

        return setting;
    }

}
