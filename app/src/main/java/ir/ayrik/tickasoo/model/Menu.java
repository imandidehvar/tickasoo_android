package ir.ayrik.tickasoo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Menu {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("background")
    @Expose
    private String background;
    @SerializedName("background_type")
    @Expose
    private int background_type;

    /**
     * No args constructor for use in serialization
     */
    public Menu() {
    }

    /**
     * @param id
     * @param icon
     * @param source
     * @param description
     * @param name
     */
    public Menu(int id, String name, String description, String icon, String source, String background, int background_type) {
        super();
        this.id = id;
        this.name = name;
        this.description = description;
        this.icon = icon;
        this.source = source;
        this.background = background;
        this.background_type = background_type;
    }

    public Menu withId(int id) {
        this.id = id;
        return this;
    }

    public Menu withName(String name) {
        this.name = name;
        return this;
    }

    public Menu withDescription(String description) {
        this.description = description;
        return this;
    }

    public Menu withIcon(String icon) {
        this.icon = icon;
        return this;
    }

    public Menu withSource(String source) {
        this.source = source;
        return this;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public int getBackground_type() {
        return background_type;
    }

    public void setBackground_type(int background_type) {
        this.background_type = background_type;
    }
}
