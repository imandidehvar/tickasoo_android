package ir.ayrik.tickasoo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Book {

    public final static int NOT_OPEN = 0;
    public final static int JUST_OPEN = 1;
    public final static int COMPLATE = 2;

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("sub_id")
    @Expose
    private int subId;
    @SerializedName("have_sub")
    @Expose
    private int haveSub;
    @SerializedName("book_name")
    @Expose
    private String bookName;
    @SerializedName("book_factor")
    @Expose
    private int bookFactor;
    @SerializedName("book_type")
    @Expose
    private int bookType;
    @SerializedName("book_describe")
    @Expose
    private String bookDescribe;
    @SerializedName("questions")
    @Expose
    private List<Question> questions;
    @SerializedName("background")
    @Expose
    private String background;

    //Not Serializable field
    private int book_status;

    /**
     * No args constructor for use in serialization
     *
     */
    public Book() {
    }

    /**
     *
     * @param id
     * @param subId
     * @param bookDescribe
     * @param bookFactor
     * @param haveSub
     * @param bookType
     * @param bookName
     */
    public Book(int id, int subId, int haveSub, String bookName, int bookFactor, int bookType, String bookDescribe, String background) {
        super();
        this.id = id;
        this.subId = subId;
        this.haveSub = haveSub;
        this.bookName = bookName;
        this.bookFactor = bookFactor;
        this.bookType = bookType;
        this.bookDescribe = bookDescribe;
        this.book_status = NOT_OPEN;
        this.background = background;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Book withId(int id) {
        this.id = id;
        return this;
    }

    public int getSubId() {
        return subId;
    }

    public void setSubId(int subId) {
        this.subId = subId;
    }

    public Book withSubId(int subId) {
        this.subId = subId;
        return this;
    }

    public int getHaveSub() {
        return haveSub;
    }

    public void setHaveSub(int haveSub) {
        this.haveSub = haveSub;
    }

    public Book withHaveSub(int haveSub) {
        this.haveSub = haveSub;
        return this;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public Book withBookName(String bookName) {
        this.bookName = bookName;
        return this;
    }

    public int getBookFactor() {
        return bookFactor;
    }

    public void setBookFactor(int bookFactor) {
        this.bookFactor = bookFactor;
    }

    public Book withBookFactor(int bookFactor) {
        this.bookFactor = bookFactor;
        return this;
    }

    public int getBookType() {
        return bookType;
    }

    public void setBookType(int bookType) {
        this.bookType = bookType;
    }

    public Book withBookType(int bookType) {
        this.bookType = bookType;
        return this;
    }

    public String getBookDescribe() {
        return bookDescribe;
    }

    public void setBookDescribe(String bookDescribe) {
        this.bookDescribe = bookDescribe;
    }

    public Book withBookDescribe(String bookDescribe) {
        this.bookDescribe = bookDescribe;
        return this;
    }

    public void setJustOpen()
    {
        this.book_status = JUST_OPEN;
    }

    public void setComplate()
    {
        this.book_status = COMPLATE;
    }

    public int get_book_status()
    {
        return this.book_status;
    }

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }
}