package ir.ayrik.tickasoo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Answer
{
    public final static int NOT_ANSWERED = 0;
    public final static int ANSWER_TRUE = 1;
    public final static int ANSWER_FALSE = 2;

    @SerializedName("user_select")
    @Expose
    private int select;
    @SerializedName("answer_status")
    @Expose
    private int status;
    @SerializedName("checked")
    @Expose
    private boolean checked;

    public Answer() {
    }

    public Answer(int select,boolean checked) {
        this.select = select;
        this.status = NOT_ANSWERED;
        this.checked = checked;
    }

    public int getSelect() {
        return select;
    }

    public void setSelect(int select) {
        this.select = select;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
