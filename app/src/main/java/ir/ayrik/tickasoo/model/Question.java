package ir.ayrik.tickasoo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Question {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("question")
    @Expose
    private String question;
    @SerializedName("answer")
    @Expose
    private String answer;
    @SerializedName("select")
    @Expose
    private int select;
    @SerializedName("user_answer")
    @Expose
    private Answer user_answer;
    /**
     * No args constructor for use in serialization
     *
     */
    public Question() {
        user_answer = new Answer();
    }

    /**
     *
     * @param id
     * @param select
     * @param answer
     * @param question
     */
    public Question(int id, String question, String answer, int select, Answer user_answer) {
        super();
        this.id = id;
        this.question = question;
        this.answer = answer;
        this.select = select;
        this.user_answer = user_answer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Question withId(int id) {
        this.id = id;
        return this;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Question withQuestion(String question) {
        this.question = question;
        return this;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Question withAnswer(String answer) {
        this.answer = answer;
        return this;
    }

    public int getSelect() {
        return select;
    }

    public void setSelect(int select) {
        this.select = select;
    }

    public Question withSelect(int select) {
        this.select = select;
        return this;
    }

    public Answer getUser_answer() {
        return user_answer;
    }

    public void setUser_answer(Answer user_answer) {
        this.user_answer = user_answer;
    }
}