package ir.ayrik.tickasoo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Konkur {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("question_count")
    @Expose
    private int questionCount;
    @SerializedName("time")
    @Expose
    private int time;

    /**
     * No args constructor for use in serialization
     *
     */
    public Konkur() {
    }

    /**
     *
     * @param id
     * @param time
     * @param title
     * @param questionCount
     */
    public Konkur(int id, String title, int questionCount, int time) {
        super();
        this.id = id;
        this.title = title;
        this.questionCount = questionCount;
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Konkur withId(int id) {
        this.id = id;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Konkur withTitle(String title) {
        this.title = title;
        return this;
    }

    public int getQuestionCount() {
        return questionCount;
    }

    public void setQuestionCount(int questionCount) {
        this.questionCount = questionCount;
    }

    public Konkur withQuestionCount(int questionCount) {
        this.questionCount = questionCount;
        return this;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public Konkur withTime(int time) {
        this.time = time;
        return this;
    }

}