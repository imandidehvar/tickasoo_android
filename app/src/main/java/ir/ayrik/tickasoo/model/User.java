package ir.ayrik.tickasoo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class User {
    @SerializedName("user_id")
    @Expose
    private int id;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("profile")
    @Expose
    private Account account;
    @SerializedName("registered_at")
    @Expose
    private Date registered_at;
    @SerializedName("active")
    @Expose
    private boolean Active;
}
