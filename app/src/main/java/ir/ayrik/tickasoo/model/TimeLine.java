package ir.ayrik.tickasoo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TimeLine {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("event_name")
    @Expose
    private String eventName;
    @SerializedName("days_count")
    @Expose
    private int daysCount;
    @SerializedName("hours_count")
    @Expose
    private int hoursCount;
    @SerializedName("minutes_count")
    @Expose
    private int minutesCount;
    @SerializedName("seconds_count")
    @Expose
    private int secondsCount;
    @SerializedName("shamsi_date")
    @Expose
    private String shamsiDate;
    @SerializedName("miladi_date")
    @Expose
    private String miladiDate;

    /**
     * No args constructor for use in serialization
     *
     */
    public TimeLine() {
    }

    /**
     *
     * @param id
     * @param minutesCount
     * @param daysCount
     * @param shamsiDate
     * @param eventName
     * @param hoursCount
     * @param miladiDate
     * @param secondsCount
     */
    public TimeLine(int id, String eventName, int daysCount, int hoursCount, int minutesCount, int secondsCount, String shamsiDate, String miladiDate) {
        super();
        this.id = id;
        this.eventName = eventName;
        this.daysCount = daysCount;
        this.hoursCount = hoursCount;
        this.minutesCount = minutesCount;
        this.secondsCount = secondsCount;
        this.shamsiDate = shamsiDate;
        this.miladiDate = miladiDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TimeLine withId(int id) {
        this.id = id;
        return this;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public TimeLine withEventName(String eventName) {
        this.eventName = eventName;
        return this;
    }

    public int getDaysCount() {
        return daysCount;
    }

    public void setDaysCount(int daysCount) {
        this.daysCount = daysCount;
    }

    public TimeLine withDaysCount(int daysCount) {
        this.daysCount = daysCount;
        return this;
    }

    public int getHoursCount() {
        return hoursCount;
    }

    public void setHoursCount(int hoursCount) {
        this.hoursCount = hoursCount;
    }

    public TimeLine withHoursCount(int hoursCount) {
        this.hoursCount = hoursCount;
        return this;
    }

    public int getMinutesCount() {
        return minutesCount;
    }

    public void setMinutesCount(int minutesCount) {
        this.minutesCount = minutesCount;
    }

    public TimeLine withMinutesCount(int minutesCount) {
        this.minutesCount = minutesCount;
        return this;
    }

    public int getSecondsCount() {
        return secondsCount;
    }

    public void setSecondsCount(int secondsCount) {
        this.secondsCount = secondsCount;
    }

    public TimeLine withSecondsCount(int secondsCount) {
        this.secondsCount = secondsCount;
        return this;
    }

    public String getShamsiDate() {
        return shamsiDate;
    }

    public void setShamsiDate(String shamsiDate) {
        this.shamsiDate = shamsiDate;
    }

    public TimeLine withShamsiDate(String shamsiDate) {
        this.shamsiDate = shamsiDate;
        return this;
    }

    public String getMiladiDate() {
        return miladiDate;
    }

    public void setMiladiDate(String miladiDate) {
        this.miladiDate = miladiDate;
    }

    public TimeLine withMiladiDate(String miladiDate) {
        this.miladiDate = miladiDate;
        return this;
    }

}
