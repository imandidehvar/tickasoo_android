package ir.ayrik.tickasoo;

import java.util.List;

import ir.ayrik.tickasoo.model.Account;
import ir.ayrik.tickasoo.model.Book;
import ir.ayrik.tickasoo.model.Chapter;
import ir.ayrik.tickasoo.model.Konkur;
import ir.ayrik.tickasoo.model.Menu;
import ir.ayrik.tickasoo.model.Setting;

public class DataHolder
{
    private static String Tokern;
    private static Current currentState;
    private static Setting setting;
    private static Account profile;
    private static List<Book> lessons;
    private static List<Book> books;
    private static List<Chapter> chapters;

    public static String getTokern() {
        return Tokern;
    }

    public static void setTokern(String tokern) {
        Tokern = tokern;
    }

    public static void setSetting(Setting setting) { DataHolder.setting = setting; }

    public static Setting getSetting() { return setting; }

    public static void setProfile(Account profile) { DataHolder.profile = profile; }

    public static Account getProfile() { return DataHolder.profile; }

    public static List<Book> getLessons() {
        return lessons;
    }

    public static void setLessons(List<Book> lessons) {
        DataHolder.lessons = lessons;
    }

    public static List<Book> getBooks() {
        return books;
    }

    public static void setBooks(List<Book> books) {
        DataHolder.books = books;
    }

    public static List<Chapter> getChapters() {
        return chapters;
    }

    public static void setChapters(List<Chapter> chapters) {
        DataHolder.chapters = chapters;
    }

    public  static Current getCurrent()
    {
        if(currentState == null)
            currentState = new Current();
        return currentState;
    }

    public static class Current{
        public Menu menu;
        public int menu_value;
        public Konkur konkur;
        public int konkur_value;
        public Book lesson;
        public int lesson_value;
        public Book book;
        public int book_value;
        public Chapter chapter;
        public int chapter_value;


    }
}
