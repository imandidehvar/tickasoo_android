package ir.ayrik.tickasoo.data.remote;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ir.ayrik.tickasoo.BuildConfig;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIProvider {
    private static APIProvider instance = null;
    private TickasooApi service;

    public APIProvider() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.1.101:8000/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        service = retrofit.create(TickasooApi.class);
    }

    public static APIProvider getInstance() {
        if (instance == null)
            instance = new APIProvider();
        return instance;
    }

    public TickasooApi getService() { return service; }
}
