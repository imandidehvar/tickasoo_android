package ir.ayrik.tickasoo.data.remote;

import java.util.Map;

import ir.ayrik.tickasoo.model.Account;
import ir.ayrik.tickasoo.model.Setting;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface TickasooApi {

    @GET("isAlive")
    Call<Setting>  getSetting();

    @POST("/login")
    Call<Account> loginUser(@Body Map map);

    @POST("/verify")
    Call<String> verifyUser(@Body Map map);
}
