package ir.ayrik.tickasoo.component.timer;

public interface ITimer
{
    public void onStart();
    public void onTick();
    public void onPause();
}
