package ir.ayrik.tickasoo.component.AyrikActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.ViewModel;

import ir.ayrik.tickasoo.R;
import ir.ayrik.tickasoo.component.timer.Timer;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public abstract class AyrikActivity<VM extends ViewModel, VB extends ViewDataBinding> extends AppCompatActivity {

    protected VM model;
    protected VB binding;

    private boolean finishWithBackPress = true;
    private boolean doubleBackPressToBack = false;
    private int backPressTries = 0;


    protected abstract int layout();

    protected int enterAnim() { return R.anim.fadein; }

    protected int exitAnim() { return R.anim.fadeout; }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, layout());
        overridePendingTransition(enterAnim(), exitAnim());
        onReady();
        new Timer(5000) {
            @Override
            public void onTick() {
                super.onTick();
                backPressTries = 0;
            }
        };
    }

    protected abstract void onReady();

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void setOnBackPressActions(boolean finishWithBackPress, boolean doubleBackPressToBack) {
        this.finishWithBackPress = finishWithBackPress;
        this.doubleBackPressToBack = doubleBackPressToBack;
    }

    public void startActivity(Class dest) {
        Intent i = new Intent(this, dest);
        startActivity(i);
    }

    public void startActivityWithFinish(Class dest) {
        finish();
        startActivity(dest);
    }

    public void startActivityWithExtraData(Class dest, Bundle bundle) {
        startActivity(new Intent(this, dest).putExtras(bundle));
    }

    public void startActivityWithResult(Class dest, int code) {
        startActivityForResult(new Intent(this, dest), code);
    }

    public void startActivityWithResultAndExtraData(Class dest, int code, Bundle bundle) {
        startActivityForResult(new Intent(this, dest).putExtras(bundle), code);
    }

    public void refresh() {
        Intent i = getIntent();
        finish();
        startActivity(i);
    }

    protected void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackPressToBack) {
            if (backPressTries == 1) {
                super.onBackPressed();
                if (finishWithBackPress)
                    finish();
            } else {
                Toast.makeText(this, "جهت بازگشت لطفا مجدد کلید بازگشت را فشار دهید", Toast.LENGTH_LONG).show();
                backPressTries++;
            }
        } else {
            super.onBackPressed();
            if (finishWithBackPress)
                finish();
        }
    }
}
