package ir.ayrik.tickasoo.component.timer;


import android.os.Handler;

public class Timer implements Runnable,ITimer
{
    private int interval;
    private Handler handler;
    private boolean status;

    public Timer(int interval) {
        this.interval = interval;
        handler = new Handler();
    }

    public void start(){
        status = true;
        onStart();
        handler.postDelayed(this,interval);
    }

    public void pause() {
        status = false;
        onPause();
        handler.removeCallbacks(this);
    }

    @Override
    public void run() {
        onTick();
        handler.postDelayed(this,interval);
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onTick() {

    }

    @Override
    public void onPause() {

    }

    public boolean isStatus() {
        return status;
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }
}
