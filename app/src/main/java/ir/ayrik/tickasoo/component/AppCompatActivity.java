package ir.ayrik.tickasoo.component;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.Gson;

import java.util.HashMap;

import ir.ayrik.tickasoo.DataHolder;
import ir.ayrik.tickasoo.R;
import ir.ayrik.tickasoo.model.Setting;
import ir.ayrik.tickasoo.util.NetworkUtils;

public abstract class AppCompatActivity<T extends ViewDataBinding> extends androidx.appcompat.app.AppCompatActivity {

    protected final String TAG = getClass().getName();
    protected T binding;
    protected Context context;
    protected TextView lbl_title;
    protected ImageView btn_back;
    private AppCompatActivityViewModel appCompatActivityViewModel;
    private boolean finishWithBackPress = true;
    private boolean doubleBackPressToBack = false;
    private byte backPressTries = 0;
    private boolean was_waitable, waiting;
    private boolean has_toolbar;


    public abstract int contentView();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setOnBackPressActions(true, false);
        binding = DataBindingUtil.setContentView(this, contentView());
        setContentView(contentView());
        appCompatActivityViewModel = ViewModelProviders.of(this).get(AppCompatActivityViewModel.class);
        context = this;

        //این برای زمانی است که در یک اکتیویتی، ویو لودینگ را اینکلود کرده باشیم که پیشفرض لودینگ نمایش داده میشود
        try {
            View view = findViewById(R.id.view_loading);
            if (view != null) {
                was_waitable = true;
                setWaiting();
            } else {
                setResponse(false);
            }
        } catch (Exception ex) {
            setResponse(false);
        }

        // این برای زمانی است که در اکتیویتی ویو تولبار را اینکلود کرده باشیم که به صورت پیشفرض باید متن عناون صفحه را از اینتنت خوانده و دکمه ی بازگشت را نیز کانفیگ کند
        try {
            View view = findViewById(R.id.inc_toolbar);
            if (view != null) {
                btn_back = view.findViewById(R.id.btn_back);
                lbl_title = view.findViewById(R.id.lbl_title);
                btn_back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        toolbar_back();
                        onBackPressed();
                    }
                });

                /*if(getIntent().hasExtra("title"))
                    lbl_title.setText(getIntent().getExtras().getString("title"));
                */

                if (!DataHolder.getCurrent().menu.getName().equals(""))
                    lbl_title.setText(DataHolder.getCurrent().menu.getName());
                has_toolbar = true;
            } else {
                has_toolbar = false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            has_toolbar = false;
        }

        // ابتدا برسی شود که ارتباط اینترنت برقرار باشد سپس در صورت برقراری ارتباط در دسترس بودن سرور برسی شود
        try {
            if (!NetworkUtils.isNetworkConnected(context)) {
                ShowToast("ارتباط با شبکه اینترنت برقرار نشد.");
            } else {
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void toolbar_back() {

    }

    public void setWaiting() {
        if (was_waitable) {
            waiting = true;
            View response = findViewById(R.id.view_response);
            response.setVisibility(View.GONE);
            View loading = findViewById(R.id.view_loading);
            loading.setVisibility(View.VISIBLE);
        }
    }

    protected boolean isWaiting() {
        return waiting;
    }

    public void setResponse() {
        waiting = false;
        View loading = findViewById(R.id.view_loading);
        loading.setVisibility(View.GONE);
        View response = findViewById(R.id.view_response);
        response.setVisibility(View.VISIBLE);
    }

    public void setResponse(boolean has_loading) {
        was_waitable = false;
        waiting = false;
    }

    public void setOnBackPressActions(boolean finishWithBackPress, boolean doubleBackPressToBack) {
        this.finishWithBackPress = finishWithBackPress;
        this.doubleBackPressToBack = doubleBackPressToBack;
    }

    public void setTitle(String title) {
        lbl_title.setText(title);
    }

    public String getTitleString() {
        return lbl_title.getText().toString();
    }

    protected abstract void onReady();

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void refresh(Object sender) {
        Intent i = getIntent();
        finish();
        startActivity(i);
    }

    public Intent startActivity(Class dest) {
        Intent i = new Intent(this, dest);
        startActivity(i);
        return i;
    }

    public Intent startActivityWithFinish(Class dest) {
        Intent i = startActivity(dest);
        finish();
        return i;
    }

    public Intent startActivityWithResult(Class dest, int code) {
        Intent i = new Intent(this, dest);
        startActivityForResult(i, code);
        return i;
    }

    public Intent startActivityWithExtraData(Class dest, Bundle bundle) {
        Intent i = new Intent(this, dest);
        i.putExtras(bundle);
        startActivity(i);
        return i;
    }

    public Intent startActivityWithFinishAndExtraData(Class dest, Bundle bundle) {
        Intent i = new Intent(this, dest);
        i.putExtras(bundle);
        startActivity(i);
        finish();
        return i;
    }

    public Intent startActivityWithResultAndExtraData(Class dest, int code, Bundle bundle) {
        Intent i = new Intent(this, dest);
        i.putExtras(bundle);
        startActivityForResult(i, code);
        return i;
    }

    protected void setBackableWithToolbar(boolean status) {
        if (has_toolbar) {
            if (status == false)
                btn_back.setVisibility(View.GONE);
            else
                btn_back.setVisibility(View.VISIBLE);
        }
    }

    public String ConvertMapToJson(HashMap<String, Object> maped) {
        Gson gson = new Gson();
        return gson.toJson(maped);
    }

    protected void ShowToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackPressToBack) {
            if (backPressTries == 1) {
                super.onBackPressed();
                if (finishWithBackPress)
                    finish();
            } else {
                Toast.makeText(context, "جهت بازگشت لطفا مجدد کلید بازگشت را فشار دهید", Toast.LENGTH_LONG).show();
                backPressTries++;
            }
        } else {
            super.onBackPressed();
            if (finishWithBackPress)
                finish();
        }
    }
}