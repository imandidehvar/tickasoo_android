package ir.ayrik.tickasoo.util;

public class Constraints {
    public static final String AUTHENTICATED = "authenticated";
    public static final String STUDENT_ID = "student_id";
    public static final String STUDENT_PHONE = "student_phone";
    public static final String STUDENT_NAME = "student_name";
    public static final String ROLE_ID = "role_id";
    public static final String ROLE_NAME = "role_name";
    public static final String MAJOR_ID = "major_id";
    public static final String MAJOR_NAME = "major_name";
    public static final String SCHOOL_ID = "school_id";
    public static final String SCHOOL_NAME = "school_name";
    public static final String CITY_ID = "city_id";
    public static final String CITY_NAME = "city_name";
}
