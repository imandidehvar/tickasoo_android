package ir.ayrik.tickasoo;

import android.content.ContextWrapper;

import androidx.multidex.MultiDex;

import com.pixplicity.easyprefs.library.Prefs;

import ir.adad.core.Adad;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class AyrikApplication extends android.app.Application {

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);

        Adad.initialize("5b850c78-0ed0-44c5-85df-aeadafd103fc");
        Adad.setTestMode(true);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/zainab.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());

        new Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(getPackageName())
                .setUseDefaultSharedPreference(true)
                .build();
    }
}
